var btCript = document.getElementById("btCript");
var btDeCript = document.getElementById("btDeCript");

btDeCript.style.display = "none";

function criptografar() {
    var texto = document.getElementById("texto").value;
    var novo_texto = "";
    //alert(texto);

    for (var i = 0; i < texto.length; i++) {
        let ch = texto[i];

        if (ch == 'a') ch = 'bala';
        else if (ch == 'e') ch = 'porta';
        else if (ch == 'i') ch = 'jasmin';
        else if (ch == 'o') ch = 'casa';
        else if (ch == 'u') ch = 'gato';

        novo_texto += ch;
        
    }
  //alert(novo_texto);
  document.getElementById("texto").value = novo_texto;
  btCript.style.display = "none";
  btDeCript.style.display = "block";
}

function descriptografar() {
  var texto = document.getElementById("texto").value;
  var p = -1;
  //alert(texto);
  var chave = ['bala', 'porta', 'jasmin', 'casa', 'gato'];
  var vogal = ['a', 'e', 'i', 'o', 'u'];
  for (var i = 0; i < chave.length; i++) {
    texto = texto.replaceAll(chave[i], vogal[i]);
  }
  //alert(texto);
  document.getElementById("texto").value = texto;
  btCript.style.display = "block";
  btDeCript.style.display = "none";
}

